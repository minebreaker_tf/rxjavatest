package sample;

import io.reactivex.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class FileCheck {

    private static final Logger logger = LoggerFactory.getLogger(FileCheck.class);

    public static void main(String[] args) {

        Path path = Paths.get(args[0]);

        AtomicReference<LocalDateTime> lastModified = new AtomicReference<>(LocalDateTime.MIN);

        Observable.interval(1, TimeUnit.SECONDS)
                  .map(unused -> Files.getLastModifiedTime(path))
                  .map(fileTime -> LocalDateTime.ofInstant(fileTime.toInstant(), ZoneId.systemDefault()))
                  .filter(t -> !t.equals(lastModified.get()))
                  .blockingSubscribe(t -> {
                      lastModified.set(t);
                      logger.info("Modified");
                  }, t -> {
                      logger.warn("Something went wrong.", t);
                  });

    }

}
