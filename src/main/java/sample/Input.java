package sample;

import io.reactivex.Observable;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public final class Input {

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Observable.<Integer>create(subscriber -> {
            while (true) {
                String line = reader.readLine();
                if (line.equals("exit") || subscriber.isDisposed()) {
                    break;
                }
                try {
                    subscriber.onNext(Integer.parseInt(line));
                } catch (NumberFormatException e) {
                    subscriber.onError(e);
                }
            }
            subscriber.onComplete();
        })
                .filter(n -> n % 2 == 0)
                .subscribe(n -> System.out.println("Number: " + n), System.err::println, () -> System.out.println("Completed!"));

    }

}
