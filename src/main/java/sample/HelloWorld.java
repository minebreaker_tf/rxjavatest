package sample;

import io.reactivex.Flowable;

public final class HelloWorld {

    public static void main(String[] args) {

        Flowable.just("hello, world")
                .subscribe(System.out::println);

    }

}
