package sample;

import io.reactivex.Observable;

public final class FizzBuzz {

    public static void main(String[] args) {

        Observable.range(1, 100)
                  .map(n -> {
                      if (n % 15 == 0) {
                          return "Fizzbuzz";
                      } else if (n % 5 == 0) {
                          return "Buzz";
                      } else if (n % 3 == 0) {
                          return "Fizz";
                      } else {
                          return n;
                      }
                  })
                  .blockingSubscribe(System.out::println);

    }

}
